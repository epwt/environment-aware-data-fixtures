<?php

namespace EPWT\EnvironmentAwareDataFixtures\Doctrine\Common\DataFixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EnvironmentAwareFixture
 * @package EPWT\EnvironmentAwareDataFixtures\Doctrine\Common\DataFixtures
 * @author Aurimas Niekis <aurimas@mailerlite.com>
 */
class EnvironmentAwareFixture implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $kernel = $this->container->get('kernel');

        switch($kernel->getEnvironment()) {
            case 'prod':
                $this->loadProduction($manager);
                break;

            case 'dev':
                $this->loadDev($manager);
                break;

            case 'test':
                $this->loadTest($manager);
                break;

            default:
                $methodName = 'load' . ucfirst($kernel->getEnvironment());

                if (!method_exists($this, $methodName)) {
                    throw new \InvalidArgumentException(
                        sprintf(
                            'There are no method for enviroment "%s" method "%s" on "%s" fixture',
                            $kernel->getEnvironment(),
                            $methodName,
                            get_class($this)
                        )
                    );
                }

                call_user_func([$this, $methodName], [$manager]);

                break;
        }
    }

    /**
     * Load data fixtures with the passed EntityManager on production environment
     *
     * @param ObjectManager $manager
     */
    public function loadProduction(ObjectManager $manager)
    {

    }

    /**
     * Load data fixtures with the passed EntityManager on development environment
     *
     * @param ObjectManager $manager
     */
    public function loadDev(ObjectManager $manager)
    {

    }

    /**
     * Load data fixtures with the passed EntityManager on test environment
     *
     * @param ObjectManager $manager
     */
    public function loadTest(ObjectManager $manager)
    {

    }
}