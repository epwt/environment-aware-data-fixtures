# Environment Aware extension for Doctrine DoctrineFixturesBundle

This extension aims to provide a simple way to manage and execute the loading of data fixtures
for the Doctrine ORM or ODM for specific environments in Symfony framework.

Default Symfony environments are supported as so. Not all methods are required to implement.

```php
namespace MyDataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use EPWT\EnvironmentAwareDataFixtures\Doctrine\Common\DataFixtures\EnvironmentAwareFixture;

class LoadUserData implements FixtureInterface
{
    public function loadProduction(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('epwt');
        $user->setPassword('production');

        $manager->persist($user);
        $manager->flush();
    }

    public function loadDev(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('epwt');
        $user->setPassword('dev');

        $manager->persist($user);
        $manager->flush();
    }

    public function loadTest(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('epwt');
        $user->setPassword('test');

        $manager->persist($user);
        $manager->flush();
    }
}
```

Custom environments also supported just make a method with prefix load for e.g.

```php
public function loadDemo(ObjectManager $manager);
```